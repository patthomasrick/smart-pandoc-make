
# Global Makefile for Pandoc

This is my personal Makefile for Pandoc, which allows me to convert all of my notes or assignments in markdown into PDFs/DOCXs/HTML files for later viewing.

# Installation

For portability, this project uses Node SASS in order to compile a SASS file into a CSS file for usage in HTML templates. In order to have this file automatically include changes, you need to install `npm` and then run

```sh
npm install
```

from inside of the repository.

# Usage

Configure `smart-pandoc.sh` to contain your path to `Makefile-markdown`. Then, set up an shell alias or add the script to a directory that is included in your PATH. Now you can invoke the script/Makefile from anywhere on your filesystem, which will create the necessary directories and output the converted files in the directory that you invoked the script from.

Should everything be set up correctly, you should be able to convert the files inside of the test directory (except for the picture of me).

```sh
cd smart-pandoc-make/tests
sh ../smart-pandoc.sh
```
